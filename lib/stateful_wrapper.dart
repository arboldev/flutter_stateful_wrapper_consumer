

import 'package:flutter/widgets.dart';

class StatefulWrapper extends StatefulWidget {

  final Widget child;
  final Function onInit;
  final Function runningCallback;

  const StatefulWrapper({Key key, this.onInit, this.child, this.runningCallback}) : super(key: key);

  @override
  _StatefulWrapperState createState() => _StatefulWrapperState();
}

class _StatefulWrapperState extends State<StatefulWrapper> {

  @override
  void initState() {
    widget.onInit();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    WidgetsBinding.instance.addPostFrameCallback((_){
      if (widget.runningCallback != null)
        widget.runningCallback();
    });

    return widget.child;
  }

}
