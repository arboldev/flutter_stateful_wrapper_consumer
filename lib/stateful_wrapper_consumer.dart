import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'stateful_wrapper.dart';

class StatefulWrapperConsumer<T> extends StatelessWidget {

  final Widget Function(BuildContext, T, Widget) builder;
  final Function(BuildContext, T) onInit;
  final Function(BuildContext, T) runningCallback;
  final Widget child;

  const StatefulWrapperConsumer({
    Key key,
    @required this.builder,
    this.onInit,
    this.child,
    this.runningCallback
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<T>(
      child: child,
      builder: (context, value, child) =>
        StatefulWrapper(
          onInit: () {
            if (onInit != null)
              onInit(context, value);
          },
          runningCallback: () {
            if (runningCallback != null)
              runningCallback(context, value);
          },
          child: Builder(
            builder: (context) {
              return builder(context, value, child);
            }
          ),
        ),
    );
  }

}
